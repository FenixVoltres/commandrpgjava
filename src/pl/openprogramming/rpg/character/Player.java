package pl.openprogramming.rpg.character;

import pl.openprogramming.rpg.character.manager.BattleCharacterManager;
import pl.openprogramming.rpg.utils.Console;

public abstract class Player extends BattleCharacter {
	private int mIntValue;

	@Override public void move(BattleCharacterManager battleCharacterManager) {
	    if (isDead())
	        return;

	    super.move(battleCharacterManager);

	    displayMoveInfo();
	    readInput();
	    attackOrDefend(battleCharacterManager);
	}

	public void displayMoveInfo() {
	    Console.displayMessageWithLineEnding("What move do you want to make?");
	    Console.displayLineEnding();
	    Console.displayMessageWithLineEnding("1: Attack!");
	    Console.displayMessageWithLineEnding("2: Defend.");
	}

	private void readInput() {
	    mIntValue  = Console.readInt();
	}

	private void attackOrDefend(BattleCharacterManager battleCharacterManager) {
	    if (mIntValue == 1)
	        attack(battleCharacterManager.character());
	    else if(mIntValue == 2)
	        defend();
	    else {
	        Console.displayLineEnding();
	        Console.displayMessageWithLineEnding("Sorry, wrong input.");
	    }
	}
}
