package pl.openprogramming.rpg.character.creator;

import pl.openprogramming.rpg.character.Dwarf;
import pl.openprogramming.rpg.character.Elf;
import pl.openprogramming.rpg.character.Player;
import pl.openprogramming.rpg.utils.Console;

public class PlayerCreator {
	private int mIntValue = 0;

	public Player create() {
	    displayCreationInfo();
	    readInput();
	    Player newPlayer = createProperCharacter();
	    return newPlayer;
	}

	private void displayCreationInfo() {
	    Console.displayMessageWithLineEnding("Which character do you want to be?");
	    Console.displayLineEnding();
	    Console.displayMessageWithLineEnding("1: An Elf");
	    Console.displayMessageWithLineEnding("2: A Dwarf");
	}

	private void readInput() {
	    mIntValue = Console.readInt();
	}

	private Player createProperCharacter() {
	    Player newPlayer = null;
	    if (mIntValue  == 1)
	        newPlayer = createElf();
	    else if(mIntValue == 2)
	        newPlayer = createDwarf();
	    else {
	        Console.displayLineEnding();
	        Console.displayMessageWithLineEnding("Sorry, wrong input.");
	    }

	    return newPlayer;
	}

	private Player createElf() {
	    Player player = new Elf();
	    return player;
	}

	private Player createDwarf() {
	    Player player = new Dwarf();
	    return player;
	}
}
