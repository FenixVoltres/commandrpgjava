package pl.openprogramming.rpg;

import pl.openprogramming.rpg.logic.Game;

public class CommandRPG {

	public static void main(String[] args) {
		Game game = new Game();
	    game.init();
	    game.start();
	}
}
