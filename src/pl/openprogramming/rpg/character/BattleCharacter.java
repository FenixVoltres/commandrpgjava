package pl.openprogramming.rpg.character;

import pl.openprogramming.rpg.character.manager.BattleCharacterManager;
import pl.openprogramming.rpg.utils.Console;

public abstract class BattleCharacter {

	protected int mLifePoints = 0;
	protected int mAttack = 0;
	protected int mDefend = 0;
	protected boolean mIsDefending = false;

	// Abstract
	public abstract String name();

	// Informators
	public boolean isDefending() {
		return mIsDefending;
	}

	public boolean isAlive() {
        boolean isAlive = (mLifePoints > 0);
        return isAlive;
    }

	public boolean isDead() {
		return !isAlive();
    }

	// Basic actions
	public void move(BattleCharacterManager other) {
	    mIsDefending = false;
	}

	public void defend() {
	    mIsDefending  = true;

	    Console.displayMessage(name());
	    Console.displayMessageWithLineEnding(" is defending");
	}

	public void attack(BattleCharacter enemy) {
	    double multiplier = 1.0;
	    if (enemy.isDefending())
	        multiplier = 0.5;

	    int damage = (int) multiplier * mAttack;
	    enemy.hit(damage);
	}

	public void hit(int damage) {
	    recalculateLifePoints(damage);
	    displayLifePointsInfo(damage);
	}

	// Private
	private void recalculateLifePoints(int damage) {
		mLifePoints -= damage;
	    if (mLifePoints < 0)
	        mLifePoints  = 0;
	}

	private void displayLifePointsInfo(int damage) {
	    Console.displayMessage(name());
	    Console.displayMessage(" got hit of ");
	    Console.displayInt(damage);
	    Console.displayMessage(" points and it's now ");
	    Console.displayInt(mLifePoints);
	    Console.displayMessageWithLineEnding(" points left.");

	    if (isDead()) {
	        Console.displayMessage(name());
	        Console.displayMessageWithLineEnding(" is dead!");
	    }
	}
}
