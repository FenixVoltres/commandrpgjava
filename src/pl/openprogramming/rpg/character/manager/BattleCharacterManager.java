package pl.openprogramming.rpg.character.manager;

import pl.openprogramming.rpg.character.BattleCharacter;

public interface BattleCharacterManager {
	public abstract void createCharacter();
	public abstract BattleCharacter character();
}
