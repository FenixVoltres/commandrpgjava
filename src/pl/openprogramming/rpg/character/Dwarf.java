package pl.openprogramming.rpg.character;

import pl.openprogramming.rpg.utils.Console;

public class Dwarf extends Player {

	public Dwarf() {
		mLifePoints = 160;
	    mAttack = 50;
	    mDefend = 50;

	    Console.displayMessageWithLineEnding("I'm a Dwarf!");
	}

	@Override public String name() {
		return "Dwarf";
	}
}
