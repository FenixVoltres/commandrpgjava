package pl.openprogramming.rpg.character;

import pl.openprogramming.rpg.utils.Console;

public class Elf extends Player {

	public Elf() {
		mLifePoints = 80;
	    mAttack = 100;
	    mDefend = 100;

	    Console.displayMessageWithLineEnding("I'm an Elf!");
	}

	@Override public String name() {
		return "Elf";
	}
}
