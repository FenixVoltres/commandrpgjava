package pl.openprogramming.rpg.logic;

import pl.openprogramming.rpg.character.manager.FoeManager;
import pl.openprogramming.rpg.character.manager.PlayerManager;
import pl.openprogramming.rpg.utils.Console;

public class TurnManager {

	private static final int TURN_LIMIT = 10;
	private int mCurrentTurnNumber = 1;

	public void nextTurn(PlayerManager playerManager, FoeManager foeManager) {
		displayTurnInfo();

	    playerManager.character().move(foeManager);
	    foeManager.character().move(playerManager);

	    ++mCurrentTurnNumber;
	}

	private void displayTurnInfo() {
		Console.displayInt(mCurrentTurnNumber);
	    Console.displayMessageWithLineEnding(" turn begins");
	}

	public boolean hasTheLastTurnPassed() {
		boolean hasLastTurnPassed = (mCurrentTurnNumber == TURN_LIMIT+1);
	    return hasLastTurnPassed;
	}

}
