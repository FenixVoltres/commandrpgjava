package pl.openprogramming.rpg.character;

import pl.openprogramming.rpg.character.manager.BattleCharacterManager;
import pl.openprogramming.rpg.utils.Globals;

public class Foe extends BattleCharacter {

	public Foe() {
	    mLifePoints = Globals.randomInt(10, 200);
	    mAttack = Globals.randomInt(10, 200);
	    mDefend = Globals.randomInt(10, 200);
	}

	@Override public String name() {
		return "Foe";
	}

	@Override public void move(BattleCharacterManager battleCharacterManager) {
	    if (isDead())
	        return;

	    super.move(battleCharacterManager);

	    int random = Globals.randomInt(0, 1);
	    boolean shouldAttack = (random == 1);

	    if (shouldAttack)
	        attack(battleCharacterManager.character());
	    else
	        defend();
	}
}
