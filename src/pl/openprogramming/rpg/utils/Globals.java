package pl.openprogramming.rpg.utils;

import java.util.Random;

public class Globals {

	public static int randomInt(int min, int max) {
		Random generator = new Random();
		int value = generator.nextInt(max-min) + min;
		return value;
	}
}
