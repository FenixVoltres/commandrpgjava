package pl.openprogramming.rpg.character.manager;

import pl.openprogramming.rpg.character.BattleCharacter;
import pl.openprogramming.rpg.character.Player;
import pl.openprogramming.rpg.character.creator.PlayerCreator;

public class PlayerManager implements BattleCharacterManager {
	private Player mPlayer = null;

	@Override public void createCharacter() {
		PlayerCreator playerCreator = new PlayerCreator();
	    mPlayer = playerCreator.create();
	}

	@Override public BattleCharacter character() {
		return mPlayer;
	}

}
