package pl.openprogramming.rpg.character.manager;

import pl.openprogramming.rpg.character.BattleCharacter;
import pl.openprogramming.rpg.character.Foe;
import pl.openprogramming.rpg.character.creator.FoeCreator;

public class FoeManager implements BattleCharacterManager {
	private Foe mFoe = null;

	@Override public void createCharacter() {
	    FoeCreator foeCreator = new FoeCreator();
	    mFoe  = foeCreator.create();
	}

	@Override public BattleCharacter character() {
	    return mFoe;
	}
}
