package pl.openprogramming.rpg.utils;

import java.util.Scanner;


public class Console {

	public static void displayMessageWithLineEnding(String message) {
		System.out.print(message);
		displayLineEnding();
	}

	public static void displayMessage(String message) {
		System.out.print(message);
	}

	public static void displayInt(int integer) {
		System.out.print(integer);
	}

	public static void displayLineEnding() {
		System.out.print("\n");
	}

	public static int readInt() {
		int value = -1;

		Scanner inScanner = new Scanner(System.in);
		value = inScanner.nextInt();

		return value;
	}
}
