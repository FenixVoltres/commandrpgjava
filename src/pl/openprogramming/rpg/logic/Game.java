package pl.openprogramming.rpg.logic;

import pl.openprogramming.rpg.character.manager.FoeManager;
import pl.openprogramming.rpg.character.manager.PlayerManager;
import pl.openprogramming.rpg.utils.Console;

public class Game {

	private final PlayerManager mPlayerManager = new PlayerManager();
	private final FoeManager mFoeManager = new FoeManager();
	private final TurnManager mTurnManager = new TurnManager();

	public void init() {
		Console.displayMessageWithLineEnding("Welcome to this superb game!");
	    mPlayerManager.createCharacter();
	    mFoeManager.createCharacter();
	}

	public void start() {
	    Console.displayMessageWithLineEnding("The game has started.");

	    while( !isOver() )
	        mTurnManager.nextTurn(mPlayerManager, mFoeManager);

	    Console.displayMessageWithLineEnding("The game is over.");

	    if (mPlayerManager.character().isAlive())
	        Console.displayMessageWithLineEnding("You won!!!");
	    else
	        Console.displayMessageWithLineEnding("You lost. :/");

	    Console.displayLineEnding();
	}

	private boolean isOver() {
		boolean isPlayerDead = mPlayerManager.character().isDead();
	    boolean isFoeDead = mFoeManager.character().isDead();
	    boolean hasTheLastTurnPassed = mTurnManager.hasTheLastTurnPassed();

	    boolean isGameOver = isPlayerDead || isFoeDead || hasTheLastTurnPassed;
	    return isGameOver;
	}

}
